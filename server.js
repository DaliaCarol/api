const mongoose = require('mongoose');
const Client = require('./models/client');
const Movement = require('./models/movimientos');
const User = require('./models/user');

var express = require('express'),
  app = express(),
  port = process.env.PORT || 3000;

//Body parser permite parsear el cuerpo de la petición
var bodyParser = require('body-parser')
app.use(bodyParser.json())
var requestjson = require('request-json');

var path = require('path');
var urlmovimientosMlab = "https://api.mlab.com/api/1/databases/bdbanca2dcvo/collections/Movimientos?apiKey=xuLbLoGiAPeA_IrvIK9TxJJp15PJoDDG";
var urlclientesMlab = "https://api.mlab.com/api/1/databases/bdbanca2dcvo/collections/Clientes?apiKey=xuLbLoGiAPeA_IrvIK9TxJJp15PJoDDG";
var urlMlabRaiz = "https://api.mlab.com/api/1/databases/bdbanca2dcvo/collections/";
var apiKey = "apiKey=xuLbLoGiAPeA_IrvIK9TxJJp15PJoDDG";
var clienteMlab = requestjson.createClient(urlmovimientosMlab);
var clienteMlab2 = requestjson.createClient(urlclientesMlab);

//conectandonos a la BD de mlab
mongoose.connect('mongodb://admin123:admin123@ds225902.mlab.com:25902/bdbanca2dcvo', function (err, res) {
    if (err){
        return console.log('Error al conectarse a la Base de Datos: ' +err)
    }
    console.log('Conexión a la Base de Datos establecida');
    app.listen(port);
    console.log('API rest corriendo en http://localhost:' + port);
 });

app.get("/", function (req, res){
  res.sendFile(path.join(__dirname, 'index.html'));
})

////////////////Métodos nuevos con coleción Cliente de mlab//////////////////////////
// clienteMlab2
//Get clientes
app.get("/client", function(req, res){
  clienteMlab2.get('', function(err, resM, body){
    if(err){
      console.log(body);
    }else{
      res.send(body);
    }
  })
})

//Devolviendo datos de TODOS los clientes con GET desde un API rest
app.get("/client2", function(req, res){
  Client.find({}, function(err, cliente){
    if(err){
      return res.status(500).send({message: 'Error al realizar la perticiòn' + err});
      //console.log(err);
    }if(!cliente){
      return res.status(404).send({message: 'No hay clientes'});
    }
    else{
      res.status(200).send(cliente);
    }
  })
})


//Devolviendo datos de un cliente con GET desde un API rest
app.get("/client2/:idCliente", function(req, res){
  let idCliente = req.params.idCliente;
  Client.findById( idCliente, function(err, cliente){
    if(err){
      return res.status(500).send({message: 'Error al realizar la perticiòn'});
      //console.log(err);
    }if(!cliente){
      return res.status(404).send({message: 'El cliente no existe'});
    }
    else{
      res.status(200).send(cliente);
    }
  })
})

//Devolviendo datos de un cliente con GET desde un API rest
app.post("/client3", function(req, res){
  console.log(req.body)
  var banca = req.body.banca;
  var idcliente = req.body.idcliente;
  console.log(banca)
  console.log(idcliente)
  Client.find( {banca: banca, idcliente: idcliente}, function(err, cliente){
    if(err){
      return res.status(500).send({message: 'Error al realizar la perticiòn'});
      //console.log(err);
    }if(!cliente){
      return res.status(404).send({message: 'El cliente no existe'});
    }
    else{
      res.status(200).send(cliente);
    }
  })
})




//Create Cliente desde API
app.post("/client", function(req, res){
  clienteMlab2.post('', req.body, function(err, resM, body){ //duda con la variable clienteMlab
    if(err){
      console.log(body);
    }else{
      console.log(body);
      res.status(200).send({message: 'El cliente se ha recibido'});
    }
  })
})

//Create Cliente desde Mongo DB
app.post("/client2", function(req, res){
console.log("------------------Creación de Cliente --------------------------")
  console.log('POST /client2')
  console.log(req.body)

  const client = new Client(req.body)


  client.save((err, clientStored)=>{
    if(err) res.status(500).send({message: 'Error al guardar en la BD' + err})

    console.log("---Cliente insertado correctamente---");
    console.log(client);
    res.status(200).send({client: clientStored})
  })
})

//Delete Cliente desde el navegador utilizando API
app.get("/clientes/deleteDesdeNavegador/:idcliente", function(req, res){
  var idcliente = req.params.idcliente
  console.log("Se solicitará borrar al cliente: " +idcliente);
  clienteMlab3 = requestjson.createClient(urlMlabRaiz + "/Clientes/" + idcliente + "?" + apiKey)
  res.send("Hemos recibido su petición de borrado para el cleiente" + idcliente);
  clienteMlab3.delete('', function(err, resM, body) {
    if(!err) {
      console.log("El cliente " +idcliente+" fue borrado exitosamente");
    }
  })
})


//Delete Cliente desde el postman utilizando API
app.delete('/clientes/delete', function(req, res) {
  var idcliente = req.headers.idcliente
  //console.log(query)
  //res.send("Se borrarà al siguiente cliente: " + req.params.idcliente);
  clienteMlab3 = requestjson.createClient(urlMlabRaiz + "/Clientes/" + idcliente + "?" + apiKey)
  clienteMlab3.delete('', function(err, resM, body) {
    if(!err) {
      res.send(body)
    }
  })
})


//Delete Cliente desde postman utilizando  Conexiòn a Mongo DB
app.delete('/client2/:idCliente', function(req, res) {
  let idCliente = req.params.idCliente
  console.log('Se intentará borrar al cliente'+idCliente)

  Client.findById( idCliente, function(err, cliente){
    if(err){
      res.status(500).send({message: 'Error al realizar la pertición' + err})
      console.log('Error al realizar la pertición' + err)
    }
    console.log('Se ha encontrado al cliente que se desea borrar: '+ cliente)
    console.log('Se intentará borrar al cliente: ' + idCliente)
    cliente.remove (function (err){
      if(err){
        res.status(500).send({message: 'Error al borrar el cliente' + err});
        console.log('Error al borrar el cliente' + err)
      }
        res.status(200).send({message: 'El cliente ' +idCliente + ' ha sido borrado'});
        //console.log('El cliente ' +idCliente + ' ha sido borrado');
    })
  })
})


//Update Cliente desde postman utilizando  Conexiòn a Mongo DB
app.put('/client2/:idCliente', function(req, res) {
  let idCliente = req.params.idCliente
  let update = req.body

  console.log('Se intentará actualizar al cliente: '+idCliente + ' utilizando el siguiente body')
  console.log(req.body)


  Client.findByIdAndUpdate( idCliente, update,function(err, clientUpdated){
    if(err){
      res.status(500).send({message: 'Error al actualizar el cliente: ' + err});
      console.log('Error al actualizar el cliente: ' + err)
    }
      res.status(200).send({message: 'El cliente ' +idCliente + ' ha sido actualizado: '});
      console.log('El cliente ' +idCliente + ' ha sido actualizado ' + clientUpdated);
  })
})

//Update Cliente desde postman utilizando  Conexiòn a Mongo DB
app.put('/agregarCuenta/:idCliente', function(req, res) {
  let idCliente = req.params.idCliente
  let num_cta = req.headers.num_cta
  let tipo_cta = req.headers.tipo_cta
  let cuerpo_actualizar = req.body

  console.log(num_cta)
  console.log(tipo_cta)
  console.log('-------------Se intentará actualizar al cliente: '+idCliente + ' utilizando el siguiente body')
  console.log(cuerpo_actualizar)
  console.log("se añadirá lo siguiente: numero de cuenta: " + num_cta + " tipo_cta: " + tipo_cta)


  Client.update( cuerpo_actualizar , { "$addToSet": {"cuentas":[{"num_cta":num_cta, "tipo_cta":tipo_cta}]} }, function(err, clientUpdated){
    if(err){
      res.status(500).send({message: 'Error al actualizar el cliente: ' + err});
      console.log('Error al actualizar el cliente: ' + err)
    }
      res.status(200).send({message: 'El cliente ' +idCliente + ' ha sido actualizado: '});
      console.log('El cliente ' +idCliente + ' ha sido actualizado ' + clientUpdated);
  })
})


//Creación de movimientos desde Mongo DB
app.post("/agregarMovimiento", function(req, res){
console.log("------------------Creación de Primer Movimiento --------------------------")
  console.log('POST /agregarMovimiento')
  console.log(req.body)

  const movement = new Movement(req.body)


  movement.save((err, movementStored)=>{
    if(err) res.status(500).send({message: 'Error al guardar en la BD' + err})

    console.log("---Movimiento insertado correctamente---");
    console.log(movement);
    res.status(200).send({movement: movementStored})
  })
})

//Obtener todos los movimientos
app.get("/getMovimientos", function(req, res){
  Movement.find({}, function(err, movement){
    if(err){
      return res.status(500).send({message: 'Error al realizar la perticiòn' + err});
      //console.log(err);
    }if(!movement){
      return res.status(404).send({message: 'No hay movimientos'});
    }
    else{
      res.status(200).send(movement);
    }
  })
})

//Get Movimiento a partir del id de la cuenta y el id del cliente
app.get("/getMovimiento", function(req, res){
  let cuenta = req.headers.cuenta
  let cliente = req.headers.cliente;
  console.log("--------------------------Obteniendo información del movimiento")

  console.log("id cuenta "+cuenta)
  console.log("id cliente " + cliente)
  Movement.findOne( {cliente:cliente, cuenta:cuenta}, function(err, movement){
  //Client.findById( idCliente, function(err, cliente){
    if(err){
      return res.status(500).send({message: 'Error al realizar la perticiòn'});
      //console.log(err);
    }if(!movement){
      return res.status(404).send({message: 'La busqueda no coincide no existe'});
    }
    else{
      res.status(200).send(movement);
    }
  })
})

/*
//Devolviendo datos de un cliente con GET desde un API rest
app.get("/client2/:idCliente", function(req, res){
  let idCliente = req.params.idCliente;
  Client.findById( idCliente, function(err, cliente){
    if(err){
      return res.status(500).send({message: 'Error al realizar la perticiòn'});
      //console.log(err);
    }if(!cliente){
      return res.status(404).send({message: 'El cliente no existe'});
    }
    else{
      res.status(200).send(cliente);
    }
  })
})
*/

//Editar Movimiento
app.put('/editarMovimiento', function(req, res) {
  let cuenta = req.headers.cuenta
  let cliente = req.headers.cliente;
  let tipo_movimiento = req.headers.tipo_movimiento
  let importe = req.headers.importe
  let cuerpo_actualizar = req.body

  console.log("Cuenta: " + cuenta + " Tipo de movimiento: " +tipo_movimiento +  " Importe: " + importe +" pesos.  Cliente: "+ cliente + " utilizando el siguiente body: ")
  console.log(cuerpo_actualizar)


  Movement.update( cuerpo_actualizar , { "$addToSet": {"movimientos":[{"tipo_movimiento":tipo_movimiento, "importe":importe}]} }, function(err, movementUpdated){
    if(err){
      res.status(500).send({message: 'Error al actualizar el cliente: ' + err});
      console.log('Error al actualizar el cliente: ' + err)
    }
      res.status(200).send({message: 'El cliente ' + cliente + ' ha sido actualizado: '});
      console.log('El cliente ' + cliente+ ' ha sido actualizado ' + movementUpdated);
  })
})






/////////////////////////Users


//Create User desde Mongo DB
app.post("/user", function(req, res){
  console.log('POST /user')
  console.log(req.body)

  const user = new User(req.body)
  console.log(user);

  user.save((err, userStored)=>{
    if(err) res.status(500).send({message: 'Error al guardar en la BD' + err})

    console.log(user);
    res.status(200).send({user: userStored})
    console.log('Usuario guardado correctamente')
  })
})



//Devolviendo datos de un usuario a partir de su nickNamey Password
app.post("/getuser", function(req, res){
  console.log(req.body)
  var nickName = req.body.nickName;
  var password = req.body.password;
  console.log(nickName)
  console.log(password)
  User.find( {nickName: nickName, password: password}, function(err, user){
    if(err){
      return res.status(500).send({message: 'Error al realizar la perticiòn'});
      //console.log(err);
    }if(!user){
      return res.status(404).send({message: 'El cliente no existe'});
    }
    else{
      res.status(200).send(user);
    }
  })
})


//Devolviendo datos de TODOS los clientes con GET desde un API rest
app.get("/getusers", function(req, res){
  User.find({}, function(err, user){
    if(err){
      return res.status(500).send({message: 'Error al realizar la perticiòn' + err});
      //console.log(err);
    }if(!user){
      return res.status(404).send({message: 'No hay usuarios'});
    }
    else{
      res.status(200).send(user);
    }
  })
})
