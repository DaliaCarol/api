'use strict'

const mongoose = require('mongoose');
const Schema = mongoose.Schema;



//Definiciòn del schema cliente
const UserSchema = new Schema({
     email: {type: String, unique: true, lowercase: true},
     nickName: {type: String, unique: true},
     password: {type: String, select: false},
     signupDate: {type: Date, default: Date.now},
     lastLogin: Date,
     estatus: Boolean
});


//Se requiere exportar el modelo para que pueda ser importado como modulo en otra parte de la aplicación
module.exports = mongoose.model('users', UserSchema ) //el nombre de la colecciòn se pone en minusculas
