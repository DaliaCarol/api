'use strict'

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

//Definiciòn del schema cliente
const MovementSchema = new Schema({
     cliente_id: String,
     cliente: Number,
     cuenta_id: String,
     cuenta: Number,
     movimientos:[{
         tipo_movimiento: {type: String, enum: ['d', 'r', 'apertura'], default: 'apertura'},
         fecha_movimiento: {type: Date, default: Date.now},
         importe: Number
       }]
});

//Se requiere exportar el modelo para que pueda ser importado como modulo en otra parte de la aplicación
module.exports = mongoose.model('movimientos', MovementSchema ) //el nombre de la colecciòn se pone en minusculas
