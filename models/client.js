'use strict'

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

//Definiciòn del schema cliente
const ClientSchema = new Schema({
     idcliente: Number,
     nombre: String,
     apellido_paterno: String,
     apellido_materno: String,
     //banca: {type: String, enum: ['Banca Comercial', 'Patrimonial y Privada', 'Banca de Empresas y Gobierno', 'a'], default: 'Banca Comercial'},
     banca: {type: String, default: 'Banca Comercial'},
     //tipo_persona: {type:String, enum:['Persona Física', 'Persona Moral', 'a', 'por definir'], default: 'Persona Física'},
     tipo_persona: {type:String, default: 'Persona Física'},
     fecha_alta: {type: Date, default: Date.now},
     cuentas: [{
       num_cta: Number,
       //tipo_cta: {type: String, enum: ['cheques', 'ahorro', 'CED', 'a'], default: 'cheques'},
       tipo_cta: {type: String, default: 'cheques'},
       saldo_actual: {type: Number, default: 0},
       movimientos:[{
         //tipo_movimiento: {type: String, enum: ['d', 'r', 'apertura'], default: 'apertura'},
         tipo_movimiento: {type: String, default: 'apertura'},
         fecha_movimiento: {type: Date, default: Date.now},
         importe: Number
       }]
     }
     ]
});

//Se requiere exportar el modelo para que pueda ser importado como modulo en otra parte de la aplicación
module.exports = mongoose.model('clientes', ClientSchema ) //el nombre de la colecciòn se pone en minusculas
